package com.cxyzy.myapm.startup

object StartupUtil {
    private var appStartTime: Long = 0
    private var firstActivityCreateTime: Long = 0
    private var homeActivityShowTime: Long = 0
    private var coldStartTime: Long = 0
    private var hotStartTime: Long = 0
    private var isCodeStart = true
    var startupListener: StartupListener? = null

    //启动时间是否准确，如果在出现主界面前，出现了登陆、欢迎页、权限申请等该标志应被设置为false，表示本次启动时间计算无效。
    var isStartTimeAccurate = true
    fun onAppStart() {
        appStartTime = System.currentTimeMillis()
    }

    /**
     * 记录App第一个Activity创建的时间
     */
    fun onFirstActivityCreate() {
        firstActivityCreateTime = System.currentTimeMillis()
    }

    /**
     * 记录主activity显示时间，并计算冷启动/热启动时间
     */
    fun onHomeActivityShow() {
        if (isStartTimeAccurate) {
            homeActivityShowTime = System.currentTimeMillis()
            if (isCodeStart) {
                coldStartTime = homeActivityShowTime - appStartTime
                startupListener?.onStarted(true, coldStartTime)

            } else if (firstActivityCreateTime != 0L) {
                hotStartTime = homeActivityShowTime - firstActivityCreateTime
                startupListener?.onStarted(false, hotStartTime)
            }
            //此处必须设置firstActivityCreateTime为0，否则每次主activity显示就会导致上报热启动时间。
            firstActivityCreateTime = 0
        }
        //重新将isStartTimeAccurate设置为true，下次正常进首页，就可以计算启动时间了。
        isStartTimeAccurate = true
        //能执行到这里，下次就不是冷启动了，故设置为false
        isCodeStart = false
    }
}

interface StartupListener {
    fun onStarted(isCodeStart: Boolean, timeCost: Long)
}