package com.cxyzy.myapm.mem

import java.io.Serializable

class RamInfo : Serializable {
    //可用RAM
    var availMemKb: Long = 0

    //手机总RAM
    var totalMemKb: Long = 0

    //内存占用满的阀值，超过即认为低内存运行状态，可能会Kill process
    var lowMemThresholdKb: Long = 0

    //是否低内存状态运行
    var isLowMemory = false

    constructor(availMemKb: Long, totalMemKb: Long, lowMemThresholdKb: Long, isLowMemory: Boolean) {
        this.availMemKb = availMemKb
        this.totalMemKb = totalMemKb
        this.lowMemThresholdKb = lowMemThresholdKb
        this.isLowMemory = isLowMemory
    }

    constructor() {}

    override fun toString(): String {
        return "RamMemoryInfo{" +
                "availMem=" + availMemKb +
                ", totalMem=" + totalMemKb +
                ", lowMemThreshold=" + lowMemThresholdKb +
                ", isLowMemory=" + isLowMemory +
                '}'
    }
}