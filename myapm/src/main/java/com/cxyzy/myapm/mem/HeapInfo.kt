package com.cxyzy.myapm.mem

import java.io.Serializable

class HeapInfo : Serializable {
    var freeMemKb: Long = 0
    var maxMemKb: Long = 0
    var allocatedKb: Long = 0

    constructor(freeMemKb: Long, maxMemKb: Long, allocatedKb: Long) {
        this.freeMemKb = freeMemKb
        this.maxMemKb = maxMemKb
        this.allocatedKb = allocatedKb
    }

    constructor() {}

    override fun toString(): String {
        return "HeapInfo{" +
                "freeMemKb=" + freeMemKb +
                ", maxMemKb=" + maxMemKb +
                ", allocatedKb=" + allocatedKb +
                '}'
    }
}