package com.cxyzy.myapm.mem

import java.io.Serializable

class PssInfo : Serializable {
    var totalPssKb = 0
    var dalvikPssKb = 0
    var nativePssKb = 0
    var otherPssKb = 0

    constructor(totalPssKb: Int, dalvikPssKb: Int, nativePssKb: Int, otherPssKb: Int) {
        this.totalPssKb = totalPssKb
        this.dalvikPssKb = dalvikPssKb
        this.nativePssKb = nativePssKb
        this.otherPssKb = otherPssKb
    }

    constructor() {}

    override fun toString(): String {
        return "PssInfo{" +
                "totalPss=" + totalPssKb +
                ", dalvikPss=" + dalvikPssKb +
                ", nativePss=" + nativePssKb +
                ", otherPss=" + otherPssKb +
                '}'
    }
}