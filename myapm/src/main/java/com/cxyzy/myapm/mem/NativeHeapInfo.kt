package com.cxyzy.myapm.mem

import java.io.Serializable

class NativeHeapInfo : Serializable {
    var heapFreeSizeKb: Long = 0
    var heapSizeKb: Long = 0
    var heapAllocatedKb: Long = 0
    override fun toString(): String {
        return "NativeHeapInfo{" +
                "heapFreeSizeKb=" + heapFreeSizeKb +
                ", heapSizeKb=" + heapSizeKb +
                ", heapAllocatedKb=" + heapAllocatedKb +
                '}'
    }
}