package com.cxyzy.myapm

import android.app.Service
import android.content.Intent
import android.os.Binder
import android.os.IBinder
import android.util.Log
import com.cxyzy.myapm.cpu.CpuUtil
import kotlin.concurrent.fixedRateTimer

class MyApmService : Service() {
    inner class MyBinder : Binder() {

        fun monitor() {
            fixedRateTimer("", true, 0, 1000) {
//                Log.d("MyApm", MemoryUtil.appHeapInfo.toString() + ";"
//                        + MemoryUtil.getAppNativeHeap() + ";"
//                        + MemoryUtil.getRamInfo(applicationContext).toString())

                Log.d("MyApm", CpuUtil.getCpuUsage().toString())
            }
        }
    }

    private val binder = MyBinder()
    override fun onBind(intent: Intent?): IBinder? {
        return binder
    }
}