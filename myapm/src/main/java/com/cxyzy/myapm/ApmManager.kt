package com.cxyzy.myapm

import android.app.Application
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.IBinder
import com.cxyzy.myapm.startup.StartupListener
import com.cxyzy.myapm.startup.StartupUtil

object ApmManager {
    lateinit var application: Application
    lateinit var myBinder: MyApmService.MyBinder

    private val serviceConnection: ServiceConnection = object : ServiceConnection {
        override fun onServiceConnected(name: ComponentName, binder: IBinder) {
            if (binder is MyApmService.MyBinder) {
                myBinder = binder
                myBinder.monitor()
            }
        }

        override fun onServiceDisconnected(name: ComponentName) {}
    }

    /**
     * 启动周期性监测各项指标任务
     */
    fun startMonitor(_application: Application) {
        application = _application
        val intent = Intent(application, MyApmService::class.java)
        application.bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE)
    }

    /**
     * 设置获取到启动时间的监听器
     */
    fun setStartupListener(startupListener: StartupListener) {
        StartupUtil.startupListener = startupListener
    }

    /**
     * App启动时调用，一般在Application的onCreate方法中调用
     */
    fun onAppStart() {
        StartupUtil.onAppStart()
    }

    /**
     * 应用的第一个activity创建时调用。
     * 应用的第一个activity的onCreate方法中调用
     */
    fun onFirstActivityCreate() {
        StartupUtil.onFirstActivityCreate()
    }

    /**
     * 应用的主界面显示时调用。
     * 一般在主界面activity的onWindowFocusChanged方法中当hasFocus为true时调用
     */
    fun onHomeActivityShow() {
        StartupUtil.onHomeActivityShow()
    }

    /**
     *  设置本次启动时间无效。
     *  如果在出现主界面前，出现了登陆、欢迎页、权限申请等该标志应在相应页面调用本方法，表示本次启动时间计算无效。
     */
    fun invalidAppStartTime() {
        StartupUtil.isStartTimeAccurate = false
    }
}