package com.cxyzy.myapmdemo

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.cxyzy.myapm.ApmManager

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ApmManager.onFirstActivityCreate()
        setContentView(R.layout.activity_main)
    }

    override fun onWindowFocusChanged(hasFocus: Boolean) {
        super.onWindowFocusChanged(hasFocus)
        if (hasFocus) {
            ApmManager.onHomeActivityShow()
        }
    }
}