package com.cxyzy.myapmdemo

import android.app.Application
import android.content.Context
import android.util.Log
import com.cxyzy.myapm.ApmManager
import com.cxyzy.myapm.startup.StartupListener
import com.cxyzy.myapm.startup.StartupUtil

class App : Application() {
    override fun onCreate() {
        super.onCreate()
        instance = this
        ApmManager.startMonitor(this)
        ApmManager.setStartupListener(object : StartupListener {
            override fun onStarted(isCodeStart: Boolean, timeCost: Long) {
                if (isCodeStart) {
                    Log.d("MyApm", "coldStartTime:${timeCost}ms")
                } else {
                    Log.d("MyApm", "hotStartTime:${timeCost}ms")
                }
            }
        })
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        ApmManager.onAppStart()
    }

    companion object {
        var instance: Application? = null
            private set
    }
}